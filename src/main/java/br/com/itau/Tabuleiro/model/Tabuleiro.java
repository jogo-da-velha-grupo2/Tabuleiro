package br.com.itau.Tabuleiro.model;

import java.util.Random;

public class Tabuleiro {
	
	String[][] casas;
	int idTabuleiro;

	public Tabuleiro(int linhas, int colunas){
		Random randomGenerator = new Random();
		this.idTabuleiro = randomGenerator.nextInt(Integer.MAX_VALUE);
		
		this.casas = new String[linhas][colunas];
		for(String[] linha : casas) {
			for (int i = 0; i < linha.length; i++) {
				linha[i] = Valor.VAZIO.getValor();
			}
		}
    }
	
	public String[][] getCasas(){
		return casas;
	}
	
	public  boolean setCasa(int x, int y, Valor valor) {
	    String vazio = Valor.VAZIO.getValor();

		if(! this.casas[x][y].equals(vazio)){
		    return false;
        }

	    this.casas[x][y] = valor.getValor();
		return true;
	}

	public int getIdTabuleiro() {
		return idTabuleiro;
	}

	public void setIdTabuleiro(int idTabuleiro) {
		this.idTabuleiro = idTabuleiro;
	}
	
			
	
	
	
	
}
