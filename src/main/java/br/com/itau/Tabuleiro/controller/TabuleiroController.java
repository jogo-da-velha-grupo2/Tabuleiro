package br.com.itau.Tabuleiro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.itau.Tabuleiro.model.Tabuleiro;
import br.com.itau.Tabuleiro.model.Valor;

@Controller
//recebe int x e int y (linha e colunas que representa o tamanho do tabuleiro)
//devolve o id do tabuleiro gerado.
class TabuleiroController {

	@RequestMapping(path = "/iniciar", method = RequestMethod.POST)
	@ResponseBody
	public int iniciarJogo(int linhas, int colunas) {
		Tabuleiro tabuleiro = new Tabuleiro(linhas, colunas);

		return tabuleiro.getIdTabuleiro();
	}

	@RequestMapping(path = "/setcasa", method = RequestMethod.POST)
	public @ResponseBody
	
	boolean setCasa(int x, int y, Valor valor) {
		//setarCasa(x, y, valor);
		try {
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	//recebe id do tabuleiro, posição marcada e a marca no tabuleiro (X) ou (O).
	//devolve boolean 

	//@RequestMapping("/getjogada")

	//recebe id tabuleiro
	//devolve a string com o tabuleiro preenchido

}
